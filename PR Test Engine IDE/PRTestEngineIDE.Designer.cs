﻿namespace PRTestEngineIDE
{
    partial class PRTestEngineIDE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PRTestEngineIDE));
            this.axApplicationMgr = new NationalInstruments.TestStand.Interop.UI.Ax.AxApplicationMgr();
            Storage.TestStandAx.axApplicationMgr = this.axApplicationMgr;
            this.axSequenceFileViewMgr = new NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceFileViewMgr();
            Storage.TestStandAx.axSequenceFileViewMgr = this.axSequenceFileViewMgr;
            this.axExecutionViewMgr = new NationalInstruments.TestStand.Interop.UI.Ax.AxExecutionViewMgr();
            Storage.TestStandAx.axExecutionViewMgr = this.axExecutionViewMgr;
            this.axInsertionPalette = new NationalInstruments.TestStand.Interop.UI.Ax.AxInsertionPalette();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.executionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sourceControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.axSequenceView = new NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceView();
            this.axVariablesView = new NationalInstruments.TestStand.Interop.UI.Ax.AxVariablesView();
            this.axSequenceList = new NationalInstruments.TestStand.Interop.UI.Ax.AxListBox();
            this.axStatusBar = new NationalInstruments.TestStand.Interop.UI.Ax.AxStatusBar();
            ((System.ComponentModel.ISupportInitialize)(this.axApplicationMgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceFileViewMgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionViewMgr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axInsertionPalette)).BeginInit();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axVariablesView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axStatusBar)).BeginInit();
            this.SuspendLayout();
            // 
            // axApplicationMgr
            // 
            this.axApplicationMgr.Enabled = true;
            this.axApplicationMgr.Location = new System.Drawing.Point(13, 5);
            this.axApplicationMgr.Name = "axApplicationMgr";
            this.axApplicationMgr.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axApplicationMgr.OcxState")));
            this.axApplicationMgr.Size = new System.Drawing.Size(32, 32);
            this.axApplicationMgr.TabIndex = 0;
            // 
            // axSequenceFileViewMgr
            // 
            this.axSequenceFileViewMgr.Enabled = true;
            this.axSequenceFileViewMgr.Location = new System.Drawing.Point(35, 0);
            this.axSequenceFileViewMgr.Name = "axSequenceFileViewMgr";
            this.axSequenceFileViewMgr.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSequenceFileViewMgr.OcxState")));
            this.axSequenceFileViewMgr.Size = new System.Drawing.Size(32, 32);
            this.axSequenceFileViewMgr.TabIndex = 1;
            // 
            // axExecutionViewMgr
            // 
            this.axExecutionViewMgr.Enabled = true;
            this.axExecutionViewMgr.Location = new System.Drawing.Point(73, 0);
            this.axExecutionViewMgr.Name = "axExecutionViewMgr";
            this.axExecutionViewMgr.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axExecutionViewMgr.OcxState")));
            this.axExecutionViewMgr.Size = new System.Drawing.Size(32, 32);
            this.axExecutionViewMgr.TabIndex = 2;
            // 
            // axInsertionPalette
            // 
            this.axInsertionPalette.Location = new System.Drawing.Point(0, 38);
            this.axInsertionPalette.Name = "axInsertionPalette";
            this.axInsertionPalette.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axInsertionPalette.OcxState")));
            this.axInsertionPalette.Size = new System.Drawing.Size(202, 572);
            this.axInsertionPalette.TabIndex = 3;
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.executionToolStripMenuItem,
            this.debugToolStripMenuItem,
            this.configureToolStripMenuItem,
            this.sourceControlToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1484, 40);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(67, 36);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(78, 36);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // executionToolStripMenuItem
            // 
            this.executionToolStripMenuItem.Name = "executionToolStripMenuItem";
            this.executionToolStripMenuItem.Size = new System.Drawing.Size(130, 36);
            this.executionToolStripMenuItem.Text = "Execution";
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(99, 36);
            this.debugToolStripMenuItem.Text = "Debug";
            // 
            // configureToolStripMenuItem
            // 
            this.configureToolStripMenuItem.Name = "configureToolStripMenuItem";
            this.configureToolStripMenuItem.Size = new System.Drawing.Size(133, 36);
            this.configureToolStripMenuItem.Text = "Configure";
            // 
            // sourceControlToolStripMenuItem
            // 
            this.sourceControlToolStripMenuItem.Name = "sourceControlToolStripMenuItem";
            this.sourceControlToolStripMenuItem.Size = new System.Drawing.Size(186, 36);
            this.sourceControlToolStripMenuItem.Text = "Source Control";
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(82, 36);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(77, 36);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // axSequenceView
            // 
            this.axSequenceView.Enabled = true;
            this.axSequenceView.Location = new System.Drawing.Point(208, 38);
            this.axSequenceView.Name = "axSequenceView";
            this.axSequenceView.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSequenceView.OcxState")));
            this.axSequenceView.Size = new System.Drawing.Size(882, 572);
            this.axSequenceView.TabIndex = 5;
            // 
            // axVariablesView
            // 
            this.axVariablesView.Location = new System.Drawing.Point(1096, 226);
            this.axVariablesView.Name = "axVariablesView";
            this.axVariablesView.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axVariablesView.OcxState")));
            this.axVariablesView.Size = new System.Drawing.Size(384, 384);
            this.axVariablesView.TabIndex = 6;
            // 
            // axSequenceList
            // 
            this.axSequenceList.Location = new System.Drawing.Point(1096, 38);
            this.axSequenceList.Name = "axSequenceList";
            this.axSequenceList.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSequenceList.OcxState")));
            this.axSequenceList.Size = new System.Drawing.Size(384, 182);
            this.axSequenceList.TabIndex = 7;
            // 
            // axStatusBar
            // 
            this.axStatusBar.Enabled = true;
            this.axStatusBar.Location = new System.Drawing.Point(0, 616);
            this.axStatusBar.Name = "axStatusBar";
            this.axStatusBar.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axStatusBar.OcxState")));
            this.axStatusBar.Size = new System.Drawing.Size(1480, 42);
            this.axStatusBar.TabIndex = 8;
            this.axStatusBar.TabStop = false;
            // 
            // PRTestEngineIDE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1484, 823);
            this.Controls.Add(this.axStatusBar);
            this.Controls.Add(this.axSequenceList);
            this.Controls.Add(this.axVariablesView);
            this.Controls.Add(this.axSequenceView);
            this.Controls.Add(this.axInsertionPalette);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.axApplicationMgr);
            this.Controls.Add(this.axSequenceFileViewMgr);
            this.Controls.Add(this.axExecutionViewMgr);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "PRTestEngineIDE";
            this.Text = "PR Test Engine IDE";
            ((System.ComponentModel.ISupportInitialize)(this.axApplicationMgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceFileViewMgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axExecutionViewMgr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axInsertionPalette)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axVariablesView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axSequenceList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axStatusBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NationalInstruments.TestStand.Interop.UI.Ax.AxApplicationMgr axApplicationMgr;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceFileViewMgr axSequenceFileViewMgr;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxExecutionViewMgr axExecutionViewMgr;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxInsertionPalette axInsertionPalette;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem executionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sourceControlToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceView axSequenceView;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxVariablesView axVariablesView;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxListBox axSequenceList;
        private NationalInstruments.TestStand.Interop.UI.Ax.AxStatusBar axStatusBar;
    }
}

