﻿namespace PRTestEngineIDE.Async
{
    abstract class Action
    {
        public virtual void Run(object @object = null)
        {
            Log.Insert(this, Catagory.INFO, "Execution start.");
        }
    }
}
