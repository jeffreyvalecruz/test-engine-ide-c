﻿namespace PRTestEngineIDE
{
    class InitializeCounters : Action
    {
        public override object Run(object @object = null)
        {
            base.Run(@object);
            Storage.Counter.AsyncCounter = 0;
            return null;
        }
    }
}
