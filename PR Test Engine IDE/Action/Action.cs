﻿namespace PRTestEngineIDE
{
    abstract class Action
    {
        public virtual object Run(object @object = null)
        {
            Log.Insert(this, Catagory.INFO, "Execution start.");
            return null;
        }
    }
}
