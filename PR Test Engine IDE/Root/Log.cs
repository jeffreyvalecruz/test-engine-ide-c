﻿namespace PRTestEngineIDE
{
    public enum Catagory { INFO, WARNING, ERROR }

    static class Log
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        extern static void OutputDebugStringA(string debugString);

        private static void InsertLogEntry(string LogEntry)
        {
            OutputDebugStringA(LogEntry);
            if (Storage.Collection.InternalLog.Count == 500)
            {
                Storage.Collection.InternalLog.Dequeue();
            }

            Storage.Collection.InternalLog.Enqueue(LogEntry);
        }

        public static void Insert(object caller, Catagory catagory, string debugString)
        {
            string LogEntry = "[" + caller.ToString() + "] [" + catagory.ToString() + "] " + debugString;
            InsertLogEntry(LogEntry);
        }

        public static void Insert(string caller, Catagory catagory, string debugString)
        {
            string LogEntry = "[" + caller + "] [" + catagory.ToString() + "] " + debugString;
            InsertLogEntry(LogEntry);
        }
    }
}
