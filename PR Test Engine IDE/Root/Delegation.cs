﻿namespace PRTestEngineIDE
{
    static class Delegation
    {
        private delegate object FunctionPointer(object @object = null);
        private delegate void AsyncFunctionPointer(object @object = null);
        private static System.AsyncCallback AsyncActionCallback = new System.AsyncCallback(EndInvoke);
        

        private static void EndInvoke(System.IAsyncResult result)
        {
            Root.DecrementAsyncCounter();
            Log.Insert("PRTestEngineIDE.Delegation.AsyncAction.Callback", Catagory.INFO, "[" + ((Async.Action)result.AsyncState).ToString() + "] Async. execution returned.");
        }


        public static object Invoke(Action action, object @object = null)
        {
            FunctionPointer fPtr = new FunctionPointer(action.Run);
            return fPtr.Invoke(@object);
        }

        public static void BeginInvoke(Async.Action action, object @object = null)
        {
            AsyncFunctionPointer fPtr = new AsyncFunctionPointer(action.Run);
            fPtr.BeginInvoke(@object, AsyncActionCallback, fPtr.Target);
        }
    }
}
