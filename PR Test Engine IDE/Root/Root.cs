﻿namespace PRTestEngineIDE
{
    static class Root
    {
        public static void IncrementAsyncCounter()
        {
            Storage.Counter.AsyncCounter++;
        }

        public static void DecrementAsyncCounter()
        {
            if (Storage.Counter.AsyncCounter > 0)
            {
                Storage.Counter.AsyncCounter--;
            }
            else
            {
                Log.Insert("PRTestEngineIDE.Root", Catagory.WARNING, "Decrementing async counter below zero is not permitted. Operation aborted.");
            }
        }
    }
}
