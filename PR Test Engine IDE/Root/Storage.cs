﻿namespace PRTestEngineIDE
{
    static class Storage
    {
        public struct Collection
        {
            public static System.Collections.Generic.Queue<string> InternalLog = new System.Collections.Generic.Queue<string>(500);
        }

        public struct Counter
        {
            public static int AsyncCounter { get; set; }
        }

        public struct TestStandAx
        {
            public static NationalInstruments.TestStand.Interop.UI.Ax.AxApplicationMgr axApplicationMgr { get; set; }
            public static NationalInstruments.TestStand.Interop.UI.Ax.AxSequenceFileViewMgr axSequenceFileViewMgr { get; set; }
            public static NationalInstruments.TestStand.Interop.UI.Ax.AxExecutionViewMgr axExecutionViewMgr { get; set; }
        }
    }
}
